/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

//user routes
Route.group(() => {
  Route.get('users', 'UsersController.users')
})
  .prefix('api/v1')

//categories routes
Route.group(() => {
  Route.resource('categories', 'CategoriesController')
})
  .prefix('api/v1')

//subcategories routes
Route.group(() => {
  Route.resource('subcategories', 'SubcategoriesController')
})
  .prefix('api/v1')

//shop routes
Route.group(() => {
  Route.resource('shops', 'ShopsController')
}).middleware(['vendor'])
  .prefix('api/v1')

//cart routes
Route.group(() => {
  Route.resource('cart', 'CartsController')
}).middleware(['vendor'])
  .prefix('api/v1')

//product routes
Route.group(() => {
  Route.resource('products', 'ProductsController')
  Route.get('products_by_subcategory/:id', 'ProductsController.getProductsbySubCategory')
}).middleware(['vendor'])
  .prefix('api/v1')
  
//auth routes
Route.group(() => {
  Route.post('register', 'AuthController.register')
  Route.post('login', 'AuthController.login')
})
  .prefix('api/v1')

