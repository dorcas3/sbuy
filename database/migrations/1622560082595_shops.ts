import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Shops extends BaseSchema {
  protected tableName = 'shops'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.string("shop_name", 255).notNullable()
      table.string("shop_location", 255).notNullable()
      table.timestamps(true)
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
