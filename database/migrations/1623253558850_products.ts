import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Products extends BaseSchema {
  protected tableName = 'products'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('inventory_id')
      .unsigned()
      .references('id')
      .inTable('inventories')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.integer('subcategory_id')
        .unsigned()
        .references('id')
        .inTable('subcategories')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.integer('shop_id')
        .unsigned()
        .references('id')
        .inTable('shops')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.string("p_name", 255).notNullable()
      table.string("p_desc", 255).notNullable()
      table.integer("price", 20).notNullable()
      table.string("image_path", 255).nullable()
      table.timestamps(true)
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
