import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Inventories extends BaseSchema {
  protected tableName = 'inventory'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('product_id')
      .unsigned()
      .references('id')
      .inTable('products')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.integer('quantity')
      table.timestamps(true)
    })
  }
  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
