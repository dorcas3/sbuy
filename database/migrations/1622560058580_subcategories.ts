import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Subcategories extends BaseSchema {
  protected tableName = 'subcategories'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('category_id')
        .unsigned()
        .references('id')
        .inTable('categories')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.string("scategory_name", 255).notNullable()
      table.string("scategory_desc", 255).nullable()
      table.string("image_path", 255).nullable()

      table.timestamps(true)
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
