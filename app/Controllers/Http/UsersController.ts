import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import User from 'App/Models/User'
export default class UsersController {
 
      public async users ({response,auth}: HttpContextContract) {
      
        try{
          await auth.use('api').authenticate()
          const users = await User.all()
          return users
        }
        catch{
          return response.badRequest()
        }
      } 

}
