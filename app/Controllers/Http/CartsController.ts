import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import Cart from 'App/Models/Cart'

export default class CartsController {
  public async index({ }: HttpContextContract) {
    const cart = await Cart.all()
    return cart
  }

  public async create({ }: HttpContextContract) {
  }

  public async store({ request }: HttpContextContract) {
    const product_id = request.input('product_id')
    const p_name = request.input('p_name')
    const price = request.input('price')
    const quantity = request.input('quantity')
    

    const cart = Cart.create({
      productId: product_id,
      p_name:p_name,
      price:price,
      quantity: quantity,

    });
    return cart
  }

  public async show({ }: HttpContextContract) {
  }

  public async edit({ }: HttpContextContract) {
  }

  public async update({ }: HttpContextContract) {
  }

  public async destroy({ }: HttpContextContract) {
  }
}
