import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'

export default class AuthController {
  public async register({ request, response }: HttpContextContract) {
    // const userData = request.only(['email','fname','lname','phoneNo','password'])
    const email = request.input('email')
    const fname = request.input('fname')
    const lname = request.input('lname')
    const phone_no = request.input('phone_no')
    const password = request.input('password')
    const role = request.input('roles')
    console.log(email);
    console.log(fname);
    console.log(lname);
    console.log(phone_no);
    console.log(password);
    console.log(role);


    try {
      //save user to database
      const user = await User.create({
        email: email,
        fname: fname,
        lname: lname,
        phone_no: phone_no,
        password: password,
      })
      await (await user).related('roles').create({
        role: role
      })
      console.log(user);

      return response.json({
        status: ' User created successfully',
        data: user
      })
    }
    catch (error) {
      return response.status(400).json({
        status: error,
        message: 'There was a problem creating the user, please try again'
      })
    }

  }

  public async login({ request, auth, response }: HttpContextContract) {
    const email = request.input('email')
    const password = request.input('password')

    try {
      const token = await auth.use('api').attempt(email, password)
      return token
    }
    catch {
      return response.badRequest('Invalid credentials')
    }
  }

  public async changePassword({ request, auth, response }: HttpContextContract) {
    const email = request.input('email')
    const password = request.input('password')

    try {
      const token = await auth.use('api').attempt(email, password)
      return token
    }
    catch {
      return response.badRequest('Invalid credentials')
    }
  }
}
