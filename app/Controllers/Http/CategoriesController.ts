import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import Category from 'App/Models/Category'
import Subcategory from 'App/Models/Subcategory'

export default class CategoriesController {
  public async index({ }: HttpContextContract) {

    // await auth.use('api').authenticate()
    const categories = await Category.query().preload("products")
    // console.log("hello")
    return categories

  }

  public async create({ }: HttpContextContract) {
  }

  public async store({ request }: HttpContextContract) {
    const category_name = request.input('category_name')
    const category_desc = request.input('category_desc')
    const image_path = request.input('image_path')


    const category = Category.create({
      category_name: category_name,
      category_desc: category_desc,
      image_path: image_path,

    });
    return category
  }

  public async show({ params, response }: HttpContextContract) {

    // await auth.use('api').authenticate()
    // const category = await Category.find(params.id)
    //  const category = await (await Category.findByOrFail('id', params.id)).preload('products')
    const category = await Category
      .query()
      .where('id', params.id)
      .preload('products')


    return category
  }

  public async edit({ }: HttpContextContract) {
  }

  public async update({ params, request }: HttpContextContract) {
    const category = await Category.findOrFail(params.id)
    if (category) {
      // console.log(category)
      // category.user_id = request.input('user_id')
      console.log("hello")
      category.category_name = request.input('category_name')
      // category.category_desc = request.input('category_desc')
      // category.image_path = request.input('image_path')
      category.save()
      return category
    }

  }

  public async destroy({ response, params }: HttpContextContract) {
    console.log("hello")
    // await auth.use('api').authenticate()
    const category = await Category.find(params.id)
    console.log(category)
    await category?.delete()
    response.send({
      message: "Category deleted Successfully"
    })


  }
}
