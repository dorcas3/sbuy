import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Shop from 'App/Models/Shop'
export default class ShopsController {

  public async index ({request,response}: HttpContextContract) {
    // const page = request.input('page', 1)
    // const limit = 10
    // const shops = await Shop.query().preload("user").paginate(page,limit)
    const shops = await Shop.all()
    return response.json({
      message: "All Shops",
      data: shops
    })
   
  }

  public async create ({}: HttpContextContract) {

  }

  public async store ({request}: HttpContextContract) {
    const user_id = request.input('user_id')
    const shop_name = request.input('shop_name')
    const shop_location = request.input('shop_location')

    const shop = Shop.create({
      userId:user_id,
      shop_name: shop_name,
      shop_location: shop_location,
      
    });
    return shop
  }

  public async show ({params,response}: HttpContextContract) {
    const shop = await Shop.find(params.id)
    return response.json({
      message: `${shop?.shop_name} Details`,
      data: shop
    })
  }

  public async edit ({}: HttpContextContract) {
  }

  public async update ({}: HttpContextContract) {
  }

  public async destroy ({params,response}: HttpContextContract) {
    const shop = await Shop.find(params.id)
    await shop?.delete()
    response.send({
      message: "Shop deleted Successfully"
    })
    

  }
}
