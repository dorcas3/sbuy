import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Product from 'App/Models/Product'
import Cloudinary from 'App/Services/Cloudinary'

export default class ProductsController {
  public async index({ response }: HttpContextContract) {
    const products = await Product.all()
    return response.json({
      message: "All Products",
      data: products
    })
  }

  public async create({ }: HttpContextContract) {
  }

  public async store({ request }: HttpContextContract) {
    
    let cloudinary_response = await Cloudinary.upload(request.file('image_path'))
    const inventory_id = request.input('inventory_id')
    const scategory_id = request.input('subcategory_id')
    const shop_id = request.input('shop_id')
    const p_name = request.input('p_name')
    const p_desc = request.input('p_desc')
    const price = request.input('price')
    const quantity = request.input('quantity')
    console.log(cloudinary_response.url);
    
    console.log(price);
    

    const product = Product.create({
      inventoryId: inventory_id,
      subcategoryId: scategory_id,
      shopId: shop_id,
      p_name: p_name,
      p_desc: p_desc,
      price: price,
      image_path: cloudinary_response.url
    });
    await (await product).related('inventory').create({
      quantity,
      productId:(await product).id

    })
    return product

  }


  public async show({ params, response }: HttpContextContract) {
    const product = await Product.find(params.id)

    return response.json({
      message: `${product?.p_name} Details`,
      data: product
    })

  }

  public async edit({ }: HttpContextContract) {

  }

  public async update({ request, response, params }: HttpContextContract) {
    const product = await Product.find(params.id)
    const inventory_id = request.input('inventory_id')
    const scategory_id = request.input('scategory_id')
    const shop_id = request.input('shop_id')
    const p_name = request.input('p_name')
    const p_desc = request.input('p_desc')
    const price = request.input('price')
    const image_path = request.input('image_path')

    if (product) {
      product.inventoryId = inventory_id;
      product.subcategoryId = scategory_id;
      product.shopId = shop_id;
      product.p_name = p_name;
      product.p_desc = p_desc;
      product.price = price;
      product.image_path = image_path;

      await product.save()
      return response.status(200).json({
        message: "product details updated successfully",
        data: product
      })
    }
    return product
  }

  public async destroy({ params }: HttpContextContract) {
    const product = await Product.find(params.id)
    await product?.delete()

  }

  public async getProductsbySubCategory({ params,response }: HttpContextContract) {
    // const products = await Product.query().where('subcategory_id', params.id).paginate(1,10)
    const products = await Product.findByOrFail('subcategory_id', params.id)
    
    console.log(products);
    
    return response.send(products)
  }

  
}
