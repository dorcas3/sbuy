import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import Subcategory from 'App/Models/Subcategory'

export default class SubcategoriesController {
  public async index({ request, response, auth }: HttpContextContract) {
    // await auth.use('api').authenticate()
    const page = request.input('page', 1)
    const limit = 10
  
      const subcategory = await Subcategory.query().preload("products")
      return response.send(subcategory)

  }

  public async create({ }: HttpContextContract) {
  }

  public async store({ request }: HttpContextContract) {
    const category_id = request.input('category_id')
    const scategory_name = request.input('scategory_name')
    const scategory_desc = request.input('scategory_desc')
    const image_path = request.input('image_path')
    console.log(category_id)
    console.log(scategory_name)

    const scategory = Subcategory.create({
      categoryId: category_id,
      scategory_name: scategory_name,
      scategory_desc: scategory_desc,
      image_path: image_path
    });
    return scategory
  }


  public async show({ response, params }: HttpContextContract) {
    // await auth.use('api').authenticate()
    // const scategory = await Subcategory.find(params.id)
    const scategory = await Subcategory
    .query()
    .where('id', params.id)
    .preload('products')

    return scategory
  }

  public async edit({ }: HttpContextContract) {
  }

  public async update({ }: HttpContextContract) {
  }

  public async destroy({ params, response }: HttpContextContract) {
    const scategory = await Subcategory.find(params.id)
    await scategory?.delete()
    response.send({
      message: "Subcategory deleted Successfully"
    })

  }
}
