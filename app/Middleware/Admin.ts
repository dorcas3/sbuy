import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Admin {
  public async handle ({response,auth}: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CAL
  //  if(!auth.user?.roles || auth.user.roles==3)

    await next()
  }
}


module.exports = Admin;