import { DateTime } from 'luxon'
import {
  BaseModel, column, hasMany,
  HasMany,
  hasManyThrough,
  HasManyThrough
} from '@ioc:Adonis/Lucid/Orm'
import Subcategory from 'App/Models/Subcategory'
import Product from 'App/Models/Product'
export default class Category extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  // @column()
  // public userId: number

  @column()
  public category_name: string

  @column()
  public category_desc: string

  @column()
  public image_path: string

  @hasMany(() => Subcategory)
  public subcategory: HasMany<typeof Subcategory>


  @hasManyThrough([
    () => Product,
    () => Subcategory,
  ])
  public products: HasManyThrough<typeof Product>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
