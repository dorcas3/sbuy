import { DateTime } from 'luxon'
import { BaseModel, belongsTo,column, BelongsTo,hasMany,HasMany} from '@ioc:Adonis/Lucid/Orm'
import Category from 'App/Models/Category'
import Product from 'App/Models/Product'
export default class Subcategory extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public categoryId: number;
  @column()
  public productId: number;
  @column()
  public scategory_name: string

  @column()
  public scategory_desc: string

  @column()
  public image_path: string
  
  @belongsTo(()=>Category)
  public category : BelongsTo<typeof Category>

  @hasMany(() => Product)
  public products: HasMany<typeof Product>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
