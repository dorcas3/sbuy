import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo,BelongsTo,} from '@ioc:Adonis/Lucid/Orm'
import Product from 'App/Models/Product'
export default class Cart extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public productId: number;

  @column()
  public product_id: number
  
  @column()
  public p_name: string

  @column()
  public price: number

  @column()
  public quantity: number;
  
  @belongsTo(()=>Product)
  public product : BelongsTo<typeof Product>

  // @belongsTo(() => Product, {
  //   foreignKey: 'productId', // defaults to userId
  // })
  // public product: BelongsTo<typeof Product>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
