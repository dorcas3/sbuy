import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'
import Shop from 'App/Models/Shop'
import Subcategory from 'App/Models/Subcategory'
import Inventory from 'App/Models/Inventory'
import {
  belongsTo,
  BelongsTo,
  hasOne,
  HasOne
 
} from '@ioc:Adonis/Lucid/Orm'

export default class Product extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  
  @column()
  public inventoryId: number

  @column()
  public categoryId: number

  @column()
  public subcategoryId: number

  @column()
  public shopId: number

  @column()
  public p_name: string

  @column()
  public p_desc: string

  @column()
  public price: number

  @column()
  public image_path: string

  @hasOne(() => Inventory)
  public inventory: HasOne<typeof Inventory>

  @belongsTo(() => Shop)
  public shop: BelongsTo<typeof Shop>;

  @belongsTo(() => Subcategory)
  public subcategory: BelongsTo<typeof Subcategory>;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
