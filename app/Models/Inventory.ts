import { DateTime } from 'luxon'
import { BaseModel, column,hasOne,HasOne } from '@ioc:Adonis/Lucid/Orm'
import Product from 'App/Models/Product'


export default class Inventory extends BaseModel {
  public static table = 'inventory'
  @column({ isPrimary: true })
  public id: number

  @column()
  public productId: number

  @column()
  public quantity: number


  @hasOne(() => Product)
  public product: HasOne<typeof Product>


  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
